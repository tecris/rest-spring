package org.demo.spring.ws.rest;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.demo.spring.ws.rest.app.model.Author;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AuthorEndpointIT {

    private static final String REST_URL = "http://localhost:8080/spring-rest/author";

    @Test
    public void test() throws JsonProcessingException {

        String firstName = "David";
        String lastName = "Mitchell";
        String about = "blue sky";

        Author author = new Author();
        author.setAbout(about);
        author.setFirstname(firstName);
        author.setLastname(lastName);
        
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(author);

        author = given().accept("application/json").contentType("application/json").body(jsonInString).accept("application/json").when().post(REST_URL).as(Author.class);

        get(REST_URL + "/" + author.getId()).then().body("firstname", equalTo(firstName)).body("lastname",
                equalTo(lastName)).body("about", equalTo(about));
    }

}
