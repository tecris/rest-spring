package org.demo.spring.ws.rest.app;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.demo.spring.ws.rest.app.model.Author;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/author")
public class AuthorController {

    private static final Logger LOGGER = Logger.getLogger(AuthorController.class.getName());

    private static final Map<Integer, Author> authorMap = new HashMap<>();

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Author> create(@RequestBody Author author) {
        LOGGER.info("Create author: " + author.toString());
        author.setId(authorMap.size());
        authorMap.put(author.getId(), author);
        return new ResponseEntity<Author>(author, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Author> findById(@PathVariable Integer id) {
        LOGGER.info("Find by author id: " + id);
        Author author = authorMap.get(id);
        LOGGER.info("Found: " + author);
        return new ResponseEntity<Author>(author, HttpStatus.OK);
    }

}
