# REST with Java & Spring

[![Circle CI](https://circleci.com/gh/tecris/ws-rest-spring.svg?style=shield)](https://circleci.com/gh/tecris/ws-rest-spring)
[![Build Status](https://travis-ci.org/tecris/ws-rest-spring.svg?branch=master)](https://travis-ci.org/tecris/ws-rest-spring)

| *Technology*  | *Version* |
| ------------- | ------------- |
| Spring boot | 1.3.2 |
| Java | 8 |
| Wildfly | 10.0.0 |
| Docker | 1.10 |
| Maven | 3.3 |


## How to run application
  * One liner
    - `$ mvn verify -Pcontinuous-delivery -Dmaven.buildNumber.doCheck=false`

  * Step-by-step

    ```
    $ docker run -d --name ws-spring-demo -p 8080:8080 -p 9990:9990 casadocker/alpine-wildfly:10.0.0
    $ mvn clean wildfly:deploy
    $ mvn clean integration-test
    ```

### Test with curl:
 - `$ ./postRequest.sh`

### WSDL
 - `http://localhost:8080/ws/scoreboard.wsdl`
